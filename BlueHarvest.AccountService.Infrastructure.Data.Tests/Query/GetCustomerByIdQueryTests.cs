﻿using AutoFixture;
using BlueHarvest.AccountService.Domain.Entity;
using BlueHarvest.AccountService.Domain.Exceptions;
using BlueHarvest.AccountService.Infrastructure.Data.Database;
using BlueHarvest.AccountService.Infrastructure.Data.Query.Command.Customers;
using BlueHarvest.AccountService.Infrastructure.Data.Query.Handler.Customers;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace BlueHarvest.AccountService.Infrastructure.Data.Tests.Query
{
    public class GetCustomerByIdQueryTests
    {
        [SetUp]
        public void Setup()
        {
            _fixture = new Fixture();
            _database = new DatabaseMock();
        }

        private IDatabaseMock _database;
        private Fixture _fixture;

        [Test]
        public void WhenGettingCustomerById_WithValidCustomerId_ShouldReturnData()
        {
            // Arrange
            var howMany = _fixture.Create<int>();
            this.CreateCustomers(howMany);

            var customer = _database.Customers.FirstOrDefault().Value;
            var handler = new GetCustomerByIdQueryHandler(_database);

            // Act
            var result = handler.Handle(new GetCustomerByIdQueryCommand(customer.Id), new CancellationToken()).Result;

            // Assert                
            Assert.IsNotNull(customer);
            Assert.AreEqual(result.Id, customer.Id);
            Assert.AreEqual(result.Name, customer.Name);
            Assert.AreEqual(result.Surname, customer.Surname);
        }

        [Test]
        public void WhenGettingCustomerById_WithInvalidCustomerId_ShouldThrowsNotFoundException()
        {
            // Arrange
            var howMany = _fixture.Create<int>();
            var id = _fixture.Create<Guid>();

            this.CreateCustomers(howMany);

            var handler = new GetCustomerByIdQueryHandler(_database);

            // Act
            var exception = Assert.ThrowsAsync<NotFoundException>(async() => await handler.Handle(new GetCustomerByIdQueryCommand(id), new CancellationToken()));

            // Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual($"Customer not found ({id})", exception.Message);
        }

        private void CreateCustomers(int howMany)
        {
            var customers = _fixture.Build<Customer>()
                .Without(x => x.Accounts)
                .CreateMany(howMany);

            foreach (var customer in customers)
            {
                var account = _fixture.Build<Account>()
                    .Without(x => x.Customer)
                    .Without(x => x.Transactions)
                    .Create();

                account.Customer = customer;

                customer.Accounts.Add(account);

                this._database.Customers.TryAdd(customer.Id, customer);
            }
        }
    }
}
