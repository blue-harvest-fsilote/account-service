﻿using AutoFixture;
using BlueHarvest.AccountService.Domain.Entity;
using BlueHarvest.AccountService.Infrastructure.Data.Database;
using BlueHarvest.AccountService.Infrastructure.Data.Query.Command.Customers;
using BlueHarvest.AccountService.Infrastructure.Data.Query.Handler.Customers;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace BlueHarvest.AccountService.Infrastructure.Data.Tests.Query
{
    public class GetAllCustomersQueryTests
    {
        [SetUp]
        public void Setup()
        {
            _fixture = new Fixture();
            _database = new DatabaseMock();
        }

        private IDatabaseMock _database;
        private Fixture _fixture;

        [Test]
        public void WhenGettingAllCustomers_WithRecordsInDatabase_ShouldReturnAll()
        {
            // Arrange
            var howMany = _fixture.Create<int>();
            this.CreateCustomers(howMany);

            var handler = new GetAllCustomersQueryHandler(_database);

            // Act
            var result = handler.Handle(new GetAllCustomersQueryCommand(), new CancellationToken()).Result;

            // Assert
            Assert.AreEqual(howMany, result.Count());

            foreach (var r in result)
            {
                Customer customer;
                
                Assert.IsTrue(_database.Customers.TryGetValue(r.Id, out customer));
                Assert.IsNotNull(customer);
            }
        }

        [Test]
        public void WhenGettingAllCustomers_WithNoRecordsInDatabase_ShouldReturnNone()
        {
            // Arrange
            var handler = new GetAllCustomersQueryHandler(_database);

            // Act
            var result = handler.Handle(new GetAllCustomersQueryCommand(), new CancellationToken()).Result;

            // Assert
            Assert.AreEqual(0, result.Count());
        }

        private void CreateCustomers(int howMany)
        {
            var customers = _fixture.Build<Customer>()
                .Without(x => x.Accounts)
                .CreateMany(howMany);

            foreach (var customer in customers)
            {
                var account = _fixture.Build<Account>()
                    .Without(x => x.Customer)
                    .Without(x => x.Transactions)
                    .Create();

                account.Customer = customer;

                customer.Accounts.Add(account);

                this._database.Customers.TryAdd(customer.Id, customer);
            }
        }
    }
}
