﻿using BlueHarvest.AccountService.Domain.Entity.Base;
using BlueHarvest.AccountService.Domain.Exceptions;
using System.Collections.Generic;

namespace BlueHarvest.AccountService.Domain.Entity
{
    public class Account : AbstractBaseEntity
    {
        public Customer Customer { get; set; }
        public decimal Balance { get; set; }
        
        public IList<Transaction> Transactions { get; set; } = new List<Transaction>();

        public override void Validate()
        {
            if (this.Customer == null)
            {
                throw new BusinessException("The owner of Account should be specified.");
            }
        }

        #region Setters

        public Account SetCustomer(Customer customer)
        {
            this.Customer = customer;
            return this;
        }

        public void AddTransaction(Transaction transaction)
        {
            if (transaction.Amount <= decimal.Zero)
            {
                throw new BusinessException("Transaction amount should be greater than zero.");
            }

            if (transaction.Type == TransactionType.WITHDRAWAL)
            {
                this.Balance -= transaction.Amount;
            }
            else
            {
                this.Balance += transaction.Amount;
            }

            this.Transactions.Add(transaction);
        }

        #endregion
    }
}
