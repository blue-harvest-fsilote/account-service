﻿namespace BlueHarvest.AccountService.Domain.Entity
{
    public enum TransactionType
    {
        FIRST_MOVEMENT,
        DEPOSIT,
        WITHDRAWAL
    }
}
