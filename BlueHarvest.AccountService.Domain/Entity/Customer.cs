﻿using BlueHarvest.AccountService.Domain.Entity.Base;
using BlueHarvest.AccountService.Domain.Exceptions;
using System.Collections.Generic;

namespace BlueHarvest.AccountService.Domain.Entity
{
    public class Customer : AbstractBaseEntity
    {
        public string Name { get; set; }
        public string Surname { get; set; }

        public IList<Account> Accounts { get; set; } = new List<Account>();

        #region AbstractBaseEntity

        public override void Validate()
        {
            if (string.IsNullOrEmpty(this.Name))
            {
                throw new BusinessException("The Customer name cannot be empty.");
            }

            if (string.IsNullOrEmpty(this.Surname))
            {
                throw new BusinessException("The Customer surname cannot be empty.");
            }
        }

        #endregion

        #region Setters

        public Customer SetName (string name)
        {
            this.Name = name;
            return this;
        }

        public Customer SetSurname(string surname)
        {
            this.Surname = surname;
            return this;
        }

        public void AddAccount(Account account)
        {
            this.Accounts.Add(account);
        }

        #endregion
    }
}
