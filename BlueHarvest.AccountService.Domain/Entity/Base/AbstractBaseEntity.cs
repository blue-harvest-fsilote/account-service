﻿using System;

namespace BlueHarvest.AccountService.Domain.Entity.Base
{
    public abstract class AbstractBaseEntity
    {
        public Guid Id { get; private set; } = Guid.NewGuid();

        public abstract void Validate();
    }
}
