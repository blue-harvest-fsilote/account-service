﻿using BlueHarvest.AccountService.Domain.Entity.Base;
using BlueHarvest.AccountService.Domain.Exceptions;
using System;

namespace BlueHarvest.AccountService.Domain.Entity
{
    public class Transaction : AbstractBaseEntity
    {
        #region Ctrs

        public Transaction() { }

        public Transaction(TransactionType type, decimal amount)
        {
            this.Type = type;
            this.Amount = amount;
        }

        #endregion

        public decimal Amount { get; set; }
        public TransactionType Type { get; set; }
        public Account Accout { get; set; }

        #region AbstractBaseEntity

        public override void Validate()
        {
            if (this.Amount <= decimal.Zero)
            {
                throw new BusinessException("Transaction amount should be greater than zero.");
            }
        }

        #endregion
    }
}
