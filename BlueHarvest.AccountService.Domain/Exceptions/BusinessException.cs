﻿using System;

namespace BlueHarvest.AccountService.Domain.Exceptions
{
    public class BusinessException : Exception
    {
        public BusinessException() : base() { }

        public BusinessException(string message) : base(message) { }
    }
}
