FROM mcr.microsoft.com/dotnet/aspnet:5.0

RUN mkdir -p /usr/src/app
 
COPY BlueHarvest.AccountService.Web/publish/ /usr/src/app/

RUN rm -f /usr/src/app/*.pdb
RUN rm -f /usr/src/app/appsettings.*

ENV SERVER_PORT=80
ENV ASPNETCORE_URLS="http://*:${SERVER_PORT}"
ENV DOTNET_URLS="http://*:${SERVER_PORT}"

RUN export SERVER_PORT=${SERVER_PORT}
RUN export ASPNETCORE_URLS="http://*:${SERVER_PORT}"
RUN export DOTNET_URLS="http://*:${SERVER_PORT}"

WORKDIR /usr/src/app
 
EXPOSE ${SERVER_PORT}

ENTRYPOINT [ "dotnet", "BlueHarvest.AccountService.Web.dll" ]
