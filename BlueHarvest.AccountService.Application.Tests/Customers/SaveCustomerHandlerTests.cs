﻿using AutoFixture;
using BlueHarvest.AccountService.Application.Command.Customers;
using BlueHarvest.AccountService.Application.Handler.Customers;
using BlueHarvest.AccountService.Domain.Entity;
using BlueHarvest.AccountService.Domain.Exceptions;
using BlueHarvest.AccountService.Infrastructure.Data.Database;
using BlueHarvest.AccountService.Infrastructure.Data.Repositories;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace BlueHarvest.AccountService.Application.Tests.Customers
{
    public class SaveCustomerHandlerTests
    {
        [SetUp]
        public void Setup()
        {
            _fixture = new Fixture();
            _database = new DatabaseMock();
            _repository = new CustomerRepository(_database);
        }

        private IDatabaseMock _database;
        private ICustomerRepository _repository;
        private Fixture _fixture;

        [Test]
        public void WhenCreatingNewCustomer_WithValidCustomerData_ShouldCreate()
        {
            // Arrange
            var command = _fixture.Create<SaveCustomerCommand>();

            var handler = new SaveCustomerHandler(_repository);

            // Act
            var result = handler.Handle(command, new CancellationToken()).Result;

            // Assert                
            Assert.IsNotNull(result);

            var customer = _repository.GetById(result.Id);

            Assert.IsTrue(result.Success);
            Assert.AreEqual(result.Id, customer.Id);
            Assert.AreEqual(command.Name, customer.Name);
            Assert.AreEqual(command.Surname, customer.Surname);
        }

        [Test]
        public void WhenUpdatingCustomer_WithValidCustomerData_ShouldUpdate()
        {
            // Arrange
            var howMany = _fixture.Create<int>();

            this.CreateCustomers(howMany);

            var id = _repository.GetAll().FirstOrDefault().Id;
            var command = _fixture.Create<SaveCustomerCommand>().SetId(id);            

            var handler = new SaveCustomerHandler(_repository);

            // Act
            var result = handler.Handle(command, new CancellationToken()).Result;

            // Assert                
            Assert.IsNotNull(result);

            var customer = _repository.GetById(result.Id);

            Assert.IsTrue(result.Success);
            Assert.AreEqual(result.Id, customer.Id);
            Assert.AreEqual(command.Name, customer.Name);
            Assert.AreEqual(command.Surname, customer.Surname);
        }

        [Test]
        public void WhenUpdatingCustomer_WithInvalidId_ShouldThrowsNotFoundException()
        {
            // Arrange
            var id = _fixture.Create<Guid>();
            var command = _fixture.Create<SaveCustomerCommand>().SetId(id);

            var handler = new SaveCustomerHandler(_repository);

            // Act
            var exception = Assert.ThrowsAsync<NotFoundException>(async () => await handler.Handle(command, new CancellationToken()));

            // Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual($"Customer not found ({id})", exception.Message);
        }

        private void CreateCustomers(int howMany)
        {
            var customers = _fixture.Build<Customer>()
                .Without(x => x.Accounts)
                .CreateMany(howMany);

            foreach (var customer in customers)
            {
                var account = _fixture.Build<Account>()
                    .Without(x => x.Customer)
                    .Without(x => x.Transactions)
                    .Create();

                account.Customer = customer;

                customer.Accounts.Add(account);

                this._database.Customers.TryAdd(customer.Id, customer);
            }
        }
    }
}
