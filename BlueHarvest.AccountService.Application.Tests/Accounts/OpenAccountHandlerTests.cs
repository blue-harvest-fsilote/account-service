﻿using AutoFixture;
using BlueHarvest.AccountService.Application.Command.Accounts;
using BlueHarvest.AccountService.Application.Command.Customers;
using BlueHarvest.AccountService.Application.Handler.Customers;
using BlueHarvest.AccountService.Domain.Entity;
using BlueHarvest.AccountService.Domain.Exceptions;
using BlueHarvest.AccountService.Infrastructure.Data.Database;
using BlueHarvest.AccountService.Infrastructure.Data.Repositories;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace BlueHarvest.AccountService.Application.Tests.Customers
{
    public class OpenAccountHandlerTests
    {
        [SetUp]
        public void Setup()
        {
            _fixture = new Fixture();
            _database = new DatabaseMock();
            _customerRepository = new CustomerRepository(_database);
            _accountRepository = new AccountRepository(_database);
        }

        private IDatabaseMock _database;
        private ICustomerRepository _customerRepository;
        private IAccountRepository _accountRepository;
        private Fixture _fixture;

        [Test]
        public void WhenOpeningNewAccount_WithValidCustomerIdAndNoInitialCredit_ShouldCreateWithoutTransactions()
        {
            // Arrange
            this.CreateCustomers(_fixture.Create<int>());

            var id = _customerRepository.GetAll().FirstOrDefault().Id;
            
            var command = _fixture.Build<OpenAccountCommand>()
                .With(x => x.CustomerId, id)
                .With(x => x.InitialCredit, decimal.Zero)
                .Create();

            var handler = new OpenAccountHandler(_accountRepository, _customerRepository);

            // Act
            var result = handler.Handle(command, new CancellationToken()).Result;

            // Assert                
            Assert.IsNotNull(result);

            var account = _accountRepository.GetById(result.Id);

            Assert.IsTrue(result.Success);
            Assert.AreEqual(result.Id, account.Id);
            Assert.AreEqual(account.Balance, decimal.Zero);
            Assert.AreEqual(account.Customer.Id, id);
            Assert.AreEqual(account.Transactions.Count, 0);
        }

        [Test]
        public void WhenOpeningNewAccount_WithValidCustomerIdAndInitialCredit_ShouldCreateWithTransaction()
        {
            // Arrange
            this.CreateCustomers(_fixture.Create<int>());

            var id = _customerRepository.GetAll().FirstOrDefault().Id;

            var command = _fixture.Build<OpenAccountCommand>()
                .With(x => x.CustomerId, id)
                .Create();

            var handler = new OpenAccountHandler(_accountRepository, _customerRepository);

            // Act
            var result = handler.Handle(command, new CancellationToken()).Result;

            // Assert                
            Assert.IsNotNull(result);

            var account = _accountRepository.GetById(result.Id);

            Assert.IsTrue(result.Success);
            Assert.AreEqual(result.Id, account.Id);
            Assert.AreEqual(account.Balance, command.InitialCredit);
            Assert.AreEqual(account.Customer.Id, id);
            Assert.AreEqual(account.Transactions.Count, 1);
            Assert.AreEqual(account.Transactions.FirstOrDefault().Type, TransactionType.FIRST_MOVEMENT);
            Assert.AreEqual(account.Transactions.FirstOrDefault().Amount, command.InitialCredit);
        }

        [Test]
        public void WhenOpeningNewAccount_WithInvalidCustomer_ShouldThrowsNotFoundException()
        {
            // Arrange
            var command = _fixture.Create<OpenAccountCommand>();

            var handler = new OpenAccountHandler(_accountRepository, _customerRepository);

            // Act
            var exception = Assert.ThrowsAsync<NotFoundException>(async () => await handler.Handle(command, new CancellationToken()));

            // Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual($"Customer not found ({command.CustomerId})", exception.Message);
        }

        private void CreateCustomers(int howMany)
        {
            var customers = _fixture.Build<Customer>()
                .Without(x => x.Accounts)
                .CreateMany(howMany);

            foreach (var customer in customers)
            {
                var account = _fixture.Build<Account>()
                    .Without(x => x.Customer)
                    .Without(x => x.Transactions)
                    .Create();

                account.Customer = customer;

                customer.Accounts.Add(account);

                this._database.Customers.TryAdd(customer.Id, customer);
            }
        }
    }
}
