﻿using System;

namespace BlueHarvest.AccountService.Application.Result.Accounts
{
    public class OpenAccountResult
    {
        public Guid Id { get; set; }
        public bool Success { get; set; }
    }
}
