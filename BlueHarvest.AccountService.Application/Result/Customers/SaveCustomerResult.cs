﻿using System;

namespace BlueHarvest.AccountService.Application.Result.Customers
{
    public class SaveCustomerResult
    {
        public Guid Id { get; set; }
        public bool Success { get; set; }
    }
}
