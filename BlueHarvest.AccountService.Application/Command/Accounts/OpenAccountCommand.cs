﻿using BlueHarvest.AccountService.Application.Result.Accounts;
using MediatR;
using System;

namespace BlueHarvest.AccountService.Application.Command.Accounts
{
    public class OpenAccountCommand : IRequest<OpenAccountResult>
    {
        public Guid CustomerId { get; set; }
        public decimal InitialCredit { get; set; }
    }
}
