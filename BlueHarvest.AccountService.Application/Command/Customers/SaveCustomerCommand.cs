﻿using BlueHarvest.AccountService.Application.Result.Customers;
using MediatR;
using System;

namespace BlueHarvest.AccountService.Application.Command.Customers
{
    public class SaveCustomerCommand : IRequest<SaveCustomerResult>
    {
        public Guid? Id { get; private set; }
        public string Name { get; set; }
        public string Surname { get; set; }

        public SaveCustomerCommand SetId(Guid id)
        {
            this.Id = id;
            return this;
        }
    }
}
