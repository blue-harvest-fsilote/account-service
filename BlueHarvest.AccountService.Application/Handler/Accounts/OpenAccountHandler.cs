﻿using BlueHarvest.AccountService.Application.Command.Accounts;
using BlueHarvest.AccountService.Application.Result.Accounts;
using BlueHarvest.AccountService.Domain.Entity;
using BlueHarvest.AccountService.Domain.Exceptions;
using BlueHarvest.AccountService.Infrastructure.Data.Repositories;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace BlueHarvest.AccountService.Application.Handler.Customers
{
    public class OpenAccountHandler : IRequestHandler<OpenAccountCommand, OpenAccountResult>
    {
        public OpenAccountHandler(IAccountRepository accountRepository, ICustomerRepository customerRepository)
        {
            this._accountRepository = accountRepository;
            this._customerRepository = customerRepository;
        }

        private readonly ICustomerRepository _customerRepository;
        private readonly IAccountRepository _accountRepository;

        public Task<OpenAccountResult> Handle(OpenAccountCommand request, CancellationToken cancellationToken)
        {
            return Task.Run(() =>
            {
                var customer = this._customerRepository.GetById(request.CustomerId);

                if (customer == null)
                    throw new NotFoundException($"Customer not found ({request.CustomerId})");

                var account = new Account();

                if (request.InitialCredit > decimal.Zero)
                {
                    account.AddTransaction(new Transaction(TransactionType.FIRST_MOVEMENT, request.InitialCredit));
                }

                account.SetCustomer(customer);
                customer.AddAccount(account);

                account.Validate();

                _accountRepository.SaveOrUpdate(account);
                _customerRepository.SaveOrUpdate(customer);

                return new OpenAccountResult
                {
                    Id = account.Id,
                    Success = true
                };
            });
        }
    }
}
