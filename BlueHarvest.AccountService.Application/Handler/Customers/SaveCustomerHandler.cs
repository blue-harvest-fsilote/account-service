﻿using BlueHarvest.AccountService.Application.Command.Customers;
using BlueHarvest.AccountService.Application.Result.Customers;
using BlueHarvest.AccountService.Domain.Entity;
using BlueHarvest.AccountService.Domain.Exceptions;
using BlueHarvest.AccountService.Infrastructure.Data.Repositories;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace BlueHarvest.AccountService.Application.Handler.Customers
{
    public class SaveCustomerHandler : IRequestHandler<SaveCustomerCommand, SaveCustomerResult>
    {
        public SaveCustomerHandler(ICustomerRepository repository)
        {
            this._repository = repository;
        }

        private readonly ICustomerRepository _repository;

        public Task<SaveCustomerResult> Handle(SaveCustomerCommand request, CancellationToken cancellationToken)
        {
            return Task.Run(() =>
            {
                Customer customer = null;

                if (request.Id.HasValue && request.Id != Guid.Empty)
                {
                    customer = this._repository.GetById(request.Id.Value);

                    if (customer == null)
                        throw new NotFoundException($"Customer not found ({request.Id})");
                }

                customer ??= new Customer();

                customer
                    .SetName(request.Name)
                    .SetSurname(request.Surname)
                    .Validate();

                _repository.SaveOrUpdate(customer);

                return new SaveCustomerResult
                {
                    Id = customer.Id,
                    Success = true
                };
            });
        }
    }
}
