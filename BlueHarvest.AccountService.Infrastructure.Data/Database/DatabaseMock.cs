﻿using BlueHarvest.AccountService.Domain.Entity;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace BlueHarvest.AccountService.Infrastructure.Data.Database
{
    public class DatabaseMock : IDatabaseMock
    {
        public DatabaseMock()
        {
            this.Accounts = new ConcurrentDictionary<Guid, Account>();
            this.Customers = new ConcurrentDictionary<Guid, Customer>();
        }

        public IDictionary<Guid, Account> Accounts { get; private set; }
        public IDictionary<Guid, Customer> Customers { get; private set; }
    }
}
