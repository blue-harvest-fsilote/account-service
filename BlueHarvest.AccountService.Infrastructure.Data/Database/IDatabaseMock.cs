﻿using BlueHarvest.AccountService.Domain.Entity;
using System;
using System.Collections.Generic;

namespace BlueHarvest.AccountService.Infrastructure.Data.Database
{
    public interface IDatabaseMock
    {
        IDictionary<Guid, Account> Accounts { get; }
        IDictionary<Guid, Customer> Customers { get; }
    }
}
