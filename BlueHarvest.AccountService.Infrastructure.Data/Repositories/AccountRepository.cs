﻿using BlueHarvest.AccountService.Domain.Entity;
using BlueHarvest.AccountService.Infrastructure.Data.Database;
using System;
using System.Collections.Generic;

namespace BlueHarvest.AccountService.Infrastructure.Data.Repositories
{
    public class AccountRepository : IAccountRepository
    {
        public AccountRepository(IDatabaseMock database)
        {
            this._database = database;
        }

        private readonly IDatabaseMock _database;

        public Account GetById(Guid id)
        {
            Account account;

            this._database.Accounts.TryGetValue(id, out account);

            return account;
        }

        public bool Delete(Guid id)
        {
            var success = false;

            if (this._database.Accounts.ContainsKey(id))
            {
                this._database.Accounts.Remove(id);
                success = true;
            }

            return success;
        }

        public void SaveOrUpdate(Account account)
        {
            if (!this._database.Customers.ContainsKey(account.Id))
            {
                this._database.Accounts.TryAdd(account.Id, account);
            }
            else
            {
                this._database.Accounts[account.Id] = account;
            }
        }
    }
}
