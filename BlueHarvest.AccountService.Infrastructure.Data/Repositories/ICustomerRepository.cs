﻿using BlueHarvest.AccountService.Domain.Entity;
using System;
using System.Collections.Generic;

namespace BlueHarvest.AccountService.Infrastructure.Data.Repositories
{
    public interface ICustomerRepository
    {
        IEnumerable<Customer> GetAll();
        Customer GetById(Guid id);
        bool Delete(Guid id);
        void SaveOrUpdate(Customer customer);
    }
}
