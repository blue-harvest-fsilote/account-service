﻿using BlueHarvest.AccountService.Domain.Entity;
using BlueHarvest.AccountService.Infrastructure.Data.Database;
using System;
using System.Collections.Generic;

namespace BlueHarvest.AccountService.Infrastructure.Data.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        public CustomerRepository(IDatabaseMock database)
        {
            this._database = database;
        }

        private readonly IDatabaseMock _database;

        public IEnumerable<Customer> GetAll()
        {
            return this._database.Customers.Values;
        }

        public Customer GetById(Guid id)
        {
            Customer customer;

            this._database.Customers.TryGetValue(id, out customer);

            return customer;
        }

        public bool Delete(Guid id)
        {
            var success = false;

            if (this._database.Customers.ContainsKey(id))
            {
                this._database.Customers.Remove(id);
                success = true;
            }

            return success;
        }

        public void SaveOrUpdate(Customer customer)
        {
            if (!this._database.Customers.ContainsKey(customer.Id))
            {
                this._database.Customers.TryAdd(customer.Id, customer);
            }
            else
            {
                this._database.Customers[customer.Id] = customer;
            }
        }
    }
}
