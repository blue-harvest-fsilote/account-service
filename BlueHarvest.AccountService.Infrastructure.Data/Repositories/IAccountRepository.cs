﻿using BlueHarvest.AccountService.Domain.Entity;
using System;

namespace BlueHarvest.AccountService.Infrastructure.Data.Repositories
{
    public interface IAccountRepository
    {
        Account GetById(Guid id);
        bool Delete(Guid id);
        void SaveOrUpdate(Account account);
    }
}
