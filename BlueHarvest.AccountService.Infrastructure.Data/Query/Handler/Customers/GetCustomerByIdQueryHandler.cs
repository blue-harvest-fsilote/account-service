﻿using BlueHarvest.AccountService.Domain.Entity;
using BlueHarvest.AccountService.Domain.Exceptions;
using BlueHarvest.AccountService.Infrastructure.Data.Database;
using BlueHarvest.AccountService.Infrastructure.Data.Query.Command.Customers;
using BlueHarvest.AccountService.Infrastructure.Data.Query.Result.Customers;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BlueHarvest.AccountService.Infrastructure.Data.Query.Handler.Customers
{
    public class GetCustomerByIdQueryHandler : IRequestHandler<GetCustomerByIdQueryCommand, GetCustomerByIdQueryResult>
    {
        public GetCustomerByIdQueryHandler(IDatabaseMock database)
        {
            this._database = database;
        }

        private readonly IDatabaseMock _database;

        public Task<GetCustomerByIdQueryResult> Handle(GetCustomerByIdQueryCommand request, CancellationToken cancellationToken)
        {
            return Task.Run(() =>
            {
                Customer customer;

                return _database.Customers.TryGetValue(request.Id, out customer)
                    ? new GetCustomerByIdQueryResult
                    {
                        Id = customer.Id,
                        Name = customer.Name,
                        Surname = customer.Surname,
                        Accounts = customer.Accounts.Select(a => new GetCustomerByIdQueryResultAccount
                        {
                            Id = a.Id,
                            Balance = a.Balance,
                            Transactions = a.Transactions.Select(t => new GetCustomerByIdQueryResultTransaction
                            {
                                Id = t.Id,
                                Amount = t.Amount,
                                Type = t.Type.ToString()
                            })
                        .ToList()
                        })
                    .ToList()
                    }
                    : throw new NotFoundException($"Customer not found ({request.Id})");
            });
        }
    }
}
