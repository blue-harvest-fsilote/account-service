﻿using BlueHarvest.AccountService.Infrastructure.Data.Database;
using BlueHarvest.AccountService.Infrastructure.Data.Query.Command.Customers;
using BlueHarvest.AccountService.Infrastructure.Data.Query.Result.Customers;
using MediatR;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BlueHarvest.AccountService.Infrastructure.Data.Query.Handler.Customers
{
    public class GetAllCustomersQueryHandler : IRequestHandler<GetAllCustomersQueryCommand, IEnumerable<GetAllCustomersQueryResult>>
    {
        public GetAllCustomersQueryHandler(IDatabaseMock database)
        {
            this._database = database;
        }

        private readonly IDatabaseMock _database;

        public Task<IEnumerable<GetAllCustomersQueryResult>> Handle(GetAllCustomersQueryCommand request, CancellationToken cancellationToken)
        {
            return Task.Run(() =>
            {
                return _database.Customers.Values.Select(x => new GetAllCustomersQueryResult
                {
                    Id = x.Id,
                    Name = x.Name,
                    Surname = x.Surname,
                    Accounts = x.Accounts.Select(a => new GetAllCustomersQueryResultAccount
                    {
                        Id = a.Id,
                        Balance = a.Balance,
                        Transactions = a.Transactions.Select(t => new GetAllCustomersQueryResultTransaction
                        {
                            Id = t.Id,
                            Amount = t.Amount,
                            Type = t.Type.ToString()
                        })
                        .ToList()
                    })
                    .ToList()
                });
            });
        }
    }
}
