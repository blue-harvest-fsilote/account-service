﻿using System;
using System.Collections.Generic;

namespace BlueHarvest.AccountService.Infrastructure.Data.Query.Result.Customers
{
    public class GetCustomerByIdQueryResult
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public IList<GetCustomerByIdQueryResultAccount> Accounts { get; set; } = new List<GetCustomerByIdQueryResultAccount>();
    }

    public class GetCustomerByIdQueryResultAccount
    {
        public Guid Id { get; set; }
        public decimal Balance { get; set; }
        public IList<GetCustomerByIdQueryResultTransaction> Transactions { get; set; } = new List<GetCustomerByIdQueryResultTransaction>();
    }

    public class GetCustomerByIdQueryResultTransaction
    {
        public Guid Id { get; set; }
        public decimal Amount { get; set; }
        public string Type { get; set; }
    }
}
