﻿using System;
using System.Collections.Generic;

namespace BlueHarvest.AccountService.Infrastructure.Data.Query.Result.Customers
{
    public class GetAllCustomersQueryResult
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public IList<GetAllCustomersQueryResultAccount> Accounts { get; set; } = new List<GetAllCustomersQueryResultAccount>();
    }

    public class GetAllCustomersQueryResultAccount
    {
        public Guid Id { get; set; }
        public decimal Balance { get; set; }
        public IList<GetAllCustomersQueryResultTransaction> Transactions { get; set; } = new List<GetAllCustomersQueryResultTransaction>();
    }

    public class GetAllCustomersQueryResultTransaction
    {
        public Guid Id { get; set; }
        public decimal Amount { get; set; }
        public string Type { get; set; }
    }
}
