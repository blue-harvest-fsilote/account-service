﻿using BlueHarvest.AccountService.Infrastructure.Data.Query.Result.Customers;
using MediatR;
using System;

namespace BlueHarvest.AccountService.Infrastructure.Data.Query.Command.Customers
{
    public class GetCustomerByIdQueryCommand : IRequest<GetCustomerByIdQueryResult>
    {
        public GetCustomerByIdQueryCommand(Guid id)
        {
            this.Id = id;
        }

        public Guid Id { get; private set; }
    }
}
