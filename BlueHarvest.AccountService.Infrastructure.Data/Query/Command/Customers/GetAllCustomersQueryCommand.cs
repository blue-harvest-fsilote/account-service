﻿using BlueHarvest.AccountService.Infrastructure.Data.Query.Result.Customers;
using MediatR;
using System.Collections.Generic;

namespace BlueHarvest.AccountService.Infrastructure.Data.Query.Command.Customers
{
    public class GetAllCustomersQueryCommand : IRequest<IEnumerable<GetAllCustomersQueryResult>>
    {
    }
}
