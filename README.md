# Blue Harvest Banking Account Service

**How to run this project**

You just need to clone the repository and open the solution file in Visual Studio 2019 or higher. Set the "BlueHarvest.AccountService.Web" as your startup project. Build and run the project. A web page with Swagger will be opened so you can execute the endpoints.

**How to run unit tests**

Build the project and click on the "run" button on the Test Explorer in Visual Studio.

**Patterns and coding structure**

This project uses an Mediator pattern and it is implemented using the MediatR package. The handlers to write information such as create a new Customer belong to the Application layer while the handler used to perform queries in the database belong to the Data layer.

Database was mocked using a singleton object that has a Dictionary that simulates a database table.

Dependency injection is made using the .NET Core default container.

**Future work**

- Separate the Account Service and the Transaction Service. So we can run http requests against each other and apply Circuit Breaker pattern. Moreover we can use a resilience strategy to do retries when communication between the services are down. To do that we can use queues and messaging brokers such as RabitMQ or Amazon SNS.

- Build a Web Client in Angular 6+.
