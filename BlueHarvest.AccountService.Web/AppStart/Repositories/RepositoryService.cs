﻿using BlueHarvest.AccountService.Infrastructure.Data.Database;
using BlueHarvest.AccountService.Infrastructure.Data.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace BlueHarvest.AccountService.Web.AppStart.Repositories
{
    public static class RepositoryService
    {
        public static void ConfigureRepositories(this IServiceCollection services)
        {
            services.AddSingleton<IDatabaseMock, DatabaseMock>();

            services.AddScoped<ICustomerRepository, CustomerRepository>();
            services.AddScoped<IAccountRepository, AccountRepository>();
        }
    }
}
