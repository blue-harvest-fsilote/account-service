﻿using MediatR;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace BlueHarvest.AccountService.Web.AppStart.Services
{
    public static class MediatRService
    {
        public static void ConfigureMediatR(this IServiceCollection services)
        {
            var assemblies = new List<Assembly>();
            
            assemblies.Add(AppDomain.CurrentDomain.Load("BlueHarvest.AccountService.Application"));
            assemblies.Add(AppDomain.CurrentDomain.Load("BlueHarvest.AccountService.Infrastructure.Data"));

            services.AddMediatR(assemblies.ToArray());
        }
    }
}
