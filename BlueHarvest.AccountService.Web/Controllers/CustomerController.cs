﻿using BlueHarvest.AccountService.Application.Command.Customers;
using BlueHarvest.AccountService.Application.Result.Customers;
using BlueHarvest.AccountService.Domain.Entity;
using BlueHarvest.AccountService.Infrastructure.Data.Query.Command.Customers;
using BlueHarvest.AccountService.Infrastructure.Data.Query.Result.Customers;
using BlueHarvest.AccountService.Infrastructure.Data.Repositories;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlueHarvest.AccountService.Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CustomerController : ControllerBase
    {
        private readonly ILogger<CustomerController> _logger;
        private readonly IMediator _mediator;

        public CustomerController(ILogger<CustomerController> logger, IMediator mediator)
        {
            _logger = logger;
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IEnumerable<GetAllCustomersQueryResult>> GetAllCustomers()
        {
            return await _mediator.Send(new GetAllCustomersQueryCommand());
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetCustomerById([FromRoute] Guid id)
        {
            var customer = await _mediator.Send(new GetCustomerByIdQueryCommand(id));

            return customer != null
                ? Ok(customer)
                : NotFound();
        }

        [HttpPost]
        public async Task<SaveCustomerResult> CreateCustomer([FromBody] SaveCustomerCommand request)
        {
            return await _mediator.Send(request);
        }

        [HttpPut]
        [Route("{id}")]
        public async Task<SaveCustomerResult> CreateCustomer([FromRoute] Guid id, [FromBody] SaveCustomerCommand request)
        {
            return await _mediator.Send(request.SetId(id));
        }
    }
}
