﻿using BlueHarvest.AccountService.Application.Command.Accounts;
using BlueHarvest.AccountService.Application.Result.Accounts;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace BlueHarvest.AccountService.Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AccountController : ControllerBase
    {
        private readonly ILogger<CustomerController> _logger;
        private readonly IMediator _mediator;

        public AccountController(ILogger<CustomerController> logger, IMediator mediator)
        {
            _logger = logger;
            _mediator = mediator;
        }

        [HttpPost]
        public async Task<OpenAccountResult> OpenNewAccount([FromBody] OpenAccountCommand request)
        {
            return await _mediator.Send(request);
        }
    }
}
