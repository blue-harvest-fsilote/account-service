﻿using BlueHarvest.AccountService.Domain.Exceptions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace BlueHarvest.AccountService.Web.Middlewares
{
    public class ExceptionHandlerMiddleware
    {
        private readonly RequestDelegate _next;
        private static IWebHostEnvironment _env;

        public ExceptionHandlerMiddleware(RequestDelegate next, IWebHostEnvironment env)
        {
            _next = next;
            _env = env;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(httpContext, ex);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception ex)
        {
            var code = HttpStatusCode.InternalServerError;
            var message = "Sorry, an unexpected error has occurred.";

            var settings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                Converters = new List<JsonConverter> { new StringEnumConverter() }
            };

            if (ex is BusinessException || ex.GetBaseException() is BusinessException)
            {
                code = HttpStatusCode.BadRequest;
                message = ex.Message;
            }
            else if (ex is NotFoundException)
            {
                code = HttpStatusCode.NotFound;
                message = ex.Message;
            }

            context.Response.StatusCode = (int)code;
            context.Response.ContentType = "application/json";

            var result = new
            {
                message = message
            };

            return context.Response.WriteAsync(JsonConvert.SerializeObject(result, settings));
        }
    }
}
